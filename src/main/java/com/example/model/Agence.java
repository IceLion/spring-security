package com.example.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "agence")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"},
        allowGetters = true)
public class Agence implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String nom;

    @NotNull
    private String nom_dir;

    @NotNull
    private String adresse;

    @NotNull
    private String num_fixe;

    @NotNull
    private String num_mobile;

    @NotNull
    private String fax;



    @OneToMany(mappedBy = "agence", fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    private List<User> users;

    @OneToMany(mappedBy = "agence", fetch = FetchType.LAZY,
            cascade =
                    CascadeType.ALL
            )
    private List<Archive> archives;

    @OneToMany(mappedBy = "agence", fetch = FetchType.LAZY,
            cascade =
                    CascadeType.ALL
            )
    private List<Application> applications;

    public List<Application> getApplications() {
        return applications;
    }


    public void setApplications(List<Application> applications) {
        this.applications = applications;
    }

    public List<Archive> getArchives() {
        return archives;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNom_dir() {
        return nom_dir;
    }

    public void setNom_dir(String nom_dir) {
        this.nom_dir = nom_dir;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNum_fixe() {
        return num_fixe;
    }

    public void setNum_fixe(String num_fixe) {
        this.num_fixe = num_fixe;
    }

    public String getNum_mobile() {
        return num_mobile;
    }

    public void setNum_mobile(String num_mobile) {
        this.num_mobile = num_mobile;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void setArchives(List<Archive> archives) {
        this.archives = archives;
    }
}

