package com.example.controller;

import com.example.model.User;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class UserPrincipal extends User implements org.springframework.security.core.userdetails.UserDetails{

    public UserPrincipal() {
        super();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return false;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}
