package com.example.controller;

import com.example.model.*;
import com.example.repository.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;

@Controller
public class AdminController {
    @Autowired
    AgenceRepository agenceRepository;
    @Autowired
    ApplicationRespository applicationRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ArchiveRepository archiveRepository;
    @Autowired
    FicheRepository ficheRepository;

    // Get All agences
    @RequestMapping(path = "/admin/agence", method = RequestMethod.GET)
    public String getAllagences(Model model) {
        model.addAttribute("agences", agenceRepository.findAll());
        return "/admin/agences/list";
    }

    // Render form
    @RequestMapping(path = "/admin/agence/add")
    public String createAgence(Model model) {
        model.addAttribute("agence", new Agence());
        return "/admin/agences/edit";
    }

    // Create new agence
    @RequestMapping(path = "/admin/agence/save", method = RequestMethod.POST)
    public String saveAgence(Agence agence) {
        agenceRepository.saveAndFlush(agence);
        return "redirect:/admin/agence";
    }

    // Update agence
    @RequestMapping(path = "/admin/agence/edit/{id}", method = RequestMethod.GET)
    public String editAgence(Model model, @PathVariable("id") Long id) {
        Agence agence = agenceRepository.getOne(id);
        model.addAttribute("agence", agence);
        return "/admin/agences/edit";
    }

    // Delete agence
    @RequestMapping(path = "/admin/agence/delete/{id}", method = RequestMethod.GET)
    public String deleteAgence(@PathVariable("id") Long id) {
        agenceRepository.delete(id);
        return "redirect:/admin/agence";
    }

    // Get All apps
    @RequestMapping(path = "/admin/application", method = RequestMethod.GET)
    public String getAllApplication(Model model) {
        model.addAttribute("applications", applicationRepository.findAll());
        return "/admin/applications/list";
    }

    // Render form
    @RequestMapping(path = "/admin/application/add", method = RequestMethod.GET)
    public String createApplication(Model model) {
        model.addAttribute("app", new Application());
        model.addAttribute("agences", agenceRepository.findAll());
        return "/admin/applications/edit";
    }

    // Create new application
    @RequestMapping(path = "/admin/application/save", method = RequestMethod.POST)
    public String saveApplication(Application application) {
        applicationRepository.save(application);
        return "redirect:/admin/application";
    }

    // Update application
    @RequestMapping(path = "/admin/application/edit/{id}", method = RequestMethod.GET)
    public String editApplication(Model model, @PathVariable("id") Long id) {
        Application application = applicationRepository.getOne(id);
        model.addAttribute("agences", agenceRepository.findAll());
        model.addAttribute("app", application);
        return "/admin/applications/edit";
    }

    // Delete application
    @RequestMapping(path = "/admin/application/delete/{id}", method = RequestMethod.GET)
    public String deleteApplication(@PathVariable("id") Long id) {
        applicationRepository.delete(id);
        return "redirect:/admin/application";
    }

    // Get All users
    @RequestMapping(path = "/admin/user", method = RequestMethod.GET)
    public String getUsers(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "/admin/users/list";
    }

    @RequestMapping(path = "/admin/user/activate/{id}", method = RequestMethod.GET)
    public String activateUser(@PathVariable("id") Long id) {

        User user = userRepository.getOne(id);
        user.setActive(1);
        userRepository.save(user);
        return "redirect:/admin/user";
    }


    // Get all fiches
    @RequestMapping(path = "/admin/fiche", method = RequestMethod.GET)
    public String getAllFiches(Model model){

        model.addAttribute("fiches", ficheRepository.findAll());
        return "/admin/fiches/list";
    }

    // Get all archive
    @RequestMapping(path = "/admin/archive", method = RequestMethod.GET)
    public String getAllArchives(Model model){

        model.addAttribute("archives", archiveRepository.findAll());
        return "/admin/archives/list";
    }

    @RequestMapping(path = "/admin/fiche/status/{id}/{status}", method = RequestMethod.GET)
    public String activateFiche(@PathVariable("id") Long id,@PathVariable("status") String status) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(auth.getName());

        Fiche fiche = ficheRepository.findOne(id);
        Archive archive = new Archive();
        archive.setFiche(fiche);
        archive.setAgence(user.getAgence());
        archive.setDate_reponse(new Date());
        archiveRepository.save(archive);
        fiche.setStatus(status);

        ficheRepository.save(fiche);
        return "redirect:/admin/fiche";
    }

}
