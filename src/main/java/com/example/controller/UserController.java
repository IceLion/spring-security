package com.example.controller;

import com.example.model.Application;
import com.example.model.Role;
import com.example.model.User;
import com.example.repository.ApplicationRespository;
import com.example.repository.UserRepository;
import com.example.repository.FicheRepository;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import com.example.model.Fiche;
import com.example.repository.AgenceRepository;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@Controller
public class UserController {

    @Autowired
    FicheRepository ficheRepository;
    @Autowired
    ApplicationRespository applicationRespository;
    @Autowired
    UserRepository userRepository;



    // Get all user fiches
    @RequestMapping(path = "/user/fiche", method = RequestMethod.GET)
    public String getAllFiches(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(auth.getName());
        model.addAttribute("fiches", ficheRepository.findByEmail(user.getEmail()));
        return "/user/listfiche";
    }

    // Render fiche form
    @RequestMapping(path = "/user/fiche/add", method = RequestMethod.GET)
    public String createFiche(Model model){
        model.addAttribute("fiche", new Fiche());
        model.addAttribute("applications", applicationRespository.findAll());
        return "/user/editfiche";
    }

    // Create new fiche
    @RequestMapping(path = "/user/fiche/save", method = RequestMethod.POST)
    public String saveFiche(@Valid Fiche fiche, BindingResult bindingResult) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        User user = userRepository.findByEmail(auth.getName());


        fiche.setUser(user);
        fiche.setNom_dir_ag(user.getAgence().getNom_dir());
        fiche.setStatus("En cours");
        Collection< ? extends GrantedAuthority > auths =  auth.getAuthorities();
        String role = auths.iterator().next().toString();

        fiche.setGrade(role);
        ficheRepository.save(fiche);

        return "redirect:/user/fiche";
    }

}
