package com.example.controller;

import com.example.model.*;
import com.example.repository.*;
import org.springframework.stereotype.Controller;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.util.*;

@Controller
public class DirectorController {
    @Autowired
    FicheRepository ficheRepository;
    @Autowired
    ApplicationRespository applicationRespository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ArchiveRepository archiveRepository;

    // Get all agence fiches
    @RequestMapping(path = "/director/fiche", method = RequestMethod.GET)
    public String getAllFiches(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(auth.getName());
        model.addAttribute("fiches", ficheRepository.findByStandard(user.getAgence().getNom_dir()));
        return "/director/fiches/list";
    }

    // Get all director fiches
    @RequestMapping(path = "/director/dirfiche", method = RequestMethod.GET)
    public String getAllDirFiches(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("fiches", ficheRepository.findByAgenceDirector(auth.getName()));
        return "/director/fiches/dirlist";
    }


    // Get all archives
    @RequestMapping(path = "/director/archive", method = RequestMethod.GET)
    public String getAllArc(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("archives", archiveRepository.findAll());
        return "/director/archives/list";
    }

    // Render fiche form
    @RequestMapping(path = "/director/fiche/add", method = RequestMethod.GET)
    public String createFiche(Model model){
        model.addAttribute("fiche", new Fiche());
        model.addAttribute("applications", applicationRespository.findAll());
        return "/director/fiches/edit";
    }

    @RequestMapping(path = "/director/fiche/save", method = RequestMethod.POST)
    public String saveFiche(@Valid Fiche fiche, BindingResult bindingResult) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(auth.getName());
        fiche.setUser(user);
        fiche.setNom_dir_ag(user.getAgence().getNom_dir());
        fiche.setStatus("Disabled");
        Collection< ? extends GrantedAuthority > auths =  auth.getAuthorities();
        String role = auths.iterator().next().toString();
        fiche.setGrade(role);
        ficheRepository.save(fiche);
        return "redirect:/director/dirfiche";
    }

    // Activate fiche
    @RequestMapping(path = "/director/fiche/status/{id}/{status}", method = RequestMethod.GET)
    public String activateFiche(@PathVariable("id") Long id,@PathVariable("status") String status) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userRepository.findByEmail(auth.getName());

        Fiche fiche = ficheRepository.findOne(id);
        Archive archive = new Archive();
        archive.setFiche(fiche);
        archive.setAgence(user.getAgence());
        archive.setDate_reponse(new Date());
        archiveRepository.save(archive);
        fiche.setStatus(status);

        ficheRepository.save(fiche);
        return "redirect:/director/fiche";
    }



}
