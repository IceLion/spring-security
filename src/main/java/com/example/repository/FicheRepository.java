package com.example.repository;

import com.example.model.Fiche;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FicheRepository extends JpaRepository<Fiche, Long> {
    @Query("select  f from Fiche f where f.user.email = :email")
    List<Fiche> findByEmail(@Param("email") String email);


    @Query("select  f from Fiche f join f.user.roles r where f.nom_dir_ag = :nom_dir_ag and r.role = 'STANDARD'")
    List<Fiche> findByStandard(@Param("nom_dir_ag") String nom_dir_ag);

    @Query("select  f from Fiche f where f.user.email = :email")
    List<Fiche> findByAgenceDirector(@Param("email") String email);
}
